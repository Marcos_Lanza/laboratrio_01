package lab_01;

import javax.swing.JOptionPane;
import java.util.Random;
import static lab_01.Logic.ANSI_RED;

public class Lab_01 {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001b[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    public static void main(String[] args) {

        menu();

    }

    public static void menu() {
        boolean go = true;
        while (go == true) {
            System.out.println("_____________________________________");
            System.out.println("Bienvenido al menú principal");
            int menu = Integer.parseInt(JOptionPane.showInputDialog("Digite una opción" + "\n"
                    + "1...PPT" + "\n"
                    + "2...Cine" + "\n"
                    + "3...Sets" + "\n"
                    + "4...Salir"));
            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            System.out.println("");

            //int menu = Integer.parseInt(JOptionPane.showInputDialog("Digite una opción:"+"\n"));
            switch (menu) {
                case 1:
                    ppt();
                    System.out.println("_____________________________________");
                    System.out.println("");
                    break;

                case 2:
                    Logic c = new Logic();
                    c.generarAsientos();
                    int ac = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de asientos que desea comprar"));
                    int cont = 0;
                    while (cont < ac) {
                        cont = cont + 1;
                        String as = JOptionPane.showInputDialog("Elija un asiento");
                        if (as == ANSI_RED) {
                            System.out.println("Asinto ocupado");

                        } else {
                            c.venderAsiento(as);
                            System.out.println(c.verAsientos());
                            System.out.println("_____________________________________");
                            System.out.println("");
                        }

                    }
                    break;

                case 3:
                    sets();
                    System.out.println("_____________________________________");
                    System.out.println("");
                    break;
                case 4:
                    System.out.println("...............saliendo..............");
                    System.out.println("");
                    go = false;
                default:
                    System.out.println("Error");

            }

        }

    }

    public static void ppt() {
        System.out.println("Juego piedra-papel-tijera");
        int seleccionCompu = (int) (Math.random() * 3) + 1;
        System.out.println("La computadora ya escogio.");

        int teclado = Integer.parseInt(JOptionPane.showInputDialog("Digite una opción:[1=Piedra, 2=Papel, 3=Tijera]:"));
        int seleccionUsuario = teclado;

        System.out.print("La computadora habia escogido: ");
        switch (seleccionCompu) {
            case 1:
                System.out.println("Piedra");
                System.out.println("..................................");

                switch (seleccionUsuario) {
                    case 1:
                        System.out.println("Empate!");
                        System.out.println("..................................");

                        break;
                    case 2:
                        System.out.println("Usted gana!");
                        System.out.println("................:.................");
                        break;
                    case 3:
                        System.out.println("La computadora gana!");
                        break;
                }
                break;

            case 2:
                System.out.println("Papel");
                switch (seleccionUsuario) {
                    case 1:
                        System.out.println("La computadora gana!");
                        break;
                    case 2:
                        System.out.println("Empate!");
                        break;
                    case 3:
                        System.out.println("Usted gana!");
                        break;
                }
                break;

            case 3:
                System.out.println("Tijera");
                switch (seleccionUsuario) {
                    case 1:
                        System.out.println("Usted Ha gana :)!");
                        break;
                    case 2:
                        System.out.println("La computadora gana :(!");
                        break;
                    case 3:
                        System.out.println("Empate!");
                        break;
                }
                break;
        }

    }

    public static void sets() {
        int a = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de sets ganados por A"));
        int b = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de sets ganados por B"));
        int c = a - b;
        int d = b - a;
        int e = a + b;
        if (a == 6 && b == 0) {
            System.out.println("Gano Jugador A: ");
        } else if (b == 2 && a == 0) {
            System.out.println("Gano Jugador B:");
        } else if (a == 5 && b == 5) {
            System.out.println("Set empatado aun no termina");
        } else if (a == 6 && b == 6) {
            System.out.println("set empatado aun no termina");
        } else if (d == 2) {
            System.out.println("Gano Jugador B:");

        } else if (c == 2) {
            System.out.println("Gano Jugador A:");
        } else if (e >= 14) {
            System.out.println("Set inválido");
        } else if (d == 1 || c == 1) {
            System.out.println("el set aún no termina");

        }else if (c>2 && c<=5){
            System.out.println("Gano Jugador A:");
        }else if (d>2 && d<=5){
            System.out.println("Gano Jugador B:");
        }else if (d>2 && d<=6 && e>=8){
            System.out.println("Set inválido");
        }else if (c>2 && c<=6 && e>=8){
            System.out.println("Set inválido");
        }else if (a==7 && b==6){
            System.out.println("Gano Jugador A:");
            
        }else if (b==7 && a==6){
             System.out.println("Gano Jugador B:");
        }else if (d<=2 && a<=8){
             System.out.println("Set inválido");
        }else if (e<=2 && b<=8){
             System.out.println("Set inválido");
        }else if (a==7 && c<=5){
             System.out.println("Set inválido");
        }else if (b==7 && e<=5){
             System.out.println("Set inválido");
        }

        
        
    }

}
