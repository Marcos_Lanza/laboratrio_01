
package lab_01;


public class Entities {
    
     private String letra;
    private int numero;
    private int precio;
    private boolean estado;

    public Entities() {
    }

    public Entities(String letra, int numero, int precio) {
        this.letra = letra;
        this.numero = numero;
        this.precio = precio;
    }

    public String getNombre() {
        return letra + numero;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    
}
